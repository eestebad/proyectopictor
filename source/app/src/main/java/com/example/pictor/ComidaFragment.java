package com.example.pictor;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ComidaFragment extends Fragment {

    private ComunicationFragments cListener;

    protected ImageButton agua;
    protected ImageButton cafe;
    protected ImageButton leche;
    protected ImageButton zumo;
    protected ImageButton fruta;
    protected ImageButton galletas;
    protected ImageButton macarrones;
    protected ImageButton yogur;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public ComidaFragment() {
        // Required empty public constructor
    }

    public static ComidaFragment newInstance(String param1, String param2) {
        ComidaFragment fragment = new ComidaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_comida, container, false);

        //Initialize the ImageButtons.
        agua = view.findViewById(R.id.imageBtnAgua);
        cafe = view.findViewById(R.id.imageBtnCafe);
        leche = view.findViewById(R.id.imageBtnLeche);
        zumo = view.findViewById(R.id.imageBtnZumo);
        fruta = view.findViewById(R.id.imageBtnFruta);
        galletas = view.findViewById(R.id.imageBtnGalletas);
        macarrones = view.findViewById(R.id.imageBtnMacarrones);
        yogur = view.findViewById(R.id.imageBtnYogur);

        //Initialize the ArrayList of ImageButtons.
        List<ImageButton> imageBtnComidaList = new ArrayList<>();
        imageBtnComidaList.add(agua);
        imageBtnComidaList.add(cafe);
        imageBtnComidaList.add(leche);
        imageBtnComidaList.add(zumo);
        imageBtnComidaList.add(fruta);
        imageBtnComidaList.add(galletas);
        imageBtnComidaList.add(macarrones);
        imageBtnComidaList.add(yogur);

        //Initialize the ArrayList of Comida Pictograms.
        List<Pictogram> pictogramComidaList = new ArrayList<>();
        pictogramComidaList.add(new Pictogram(0, "agua ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fagua.png?alt=media&token=cadfedfc-5552-41b7-a48f-2650819f9c49"));
        pictogramComidaList.add(new Pictogram(1, "cafe ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fcafe_leche.png?alt=media&token=225a510c-7835-4cd0-b6d1-8448e924c816"));
        pictogramComidaList.add(new Pictogram(2, "leche ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fleche.png?alt=media&token=fdfa86e2-4d2f-4bbe-972f-d252572df5ab"));
        pictogramComidaList.add(new Pictogram(3, "zumo ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fzumo.png?alt=media&token=34a5cb47-17f2-4cb1-bc0f-5d5c80e78c9c"));
        pictogramComidaList.add(new Pictogram(0, "fruta ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Ffruta.png?alt=media&token=c6f08e00-f1aa-4c6b-9ee5-20531e1f046a"));
        pictogramComidaList.add(new Pictogram(1, "galletas ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fgalletas.png?alt=media&token=179d4c9b-5bea-4eb2-b971-8e4fbbb0cd9f"));
        pictogramComidaList.add(new Pictogram(2, "macarrones ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fmacarrones.png?alt=media&token=62611f67-1640-4823-8f1d-3af05d9abd68"));
        pictogramComidaList.add(new Pictogram(3, "yogur ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fyogur.png?alt=media&token=0dd0c532-388e-4d53-8e88-662c86c35361"));


        int count = 0;

        //Create each ImageButton with a Pictogram.
        for (ImageButton imageButton:imageBtnComidaList
        ) {
            Glide.with(this)
                    .load(pictogramComidaList.get(count).getUrlStorage())
                    .into(imageButton);
            count++;
        }

        //Call to the method to display the Pictogram's name
        agua.setOnClickListener(v -> cListener.showTextImage(v));
        cafe.setOnClickListener(v -> cListener.showTextImage(v));
        leche.setOnClickListener(v -> cListener.showTextImage(v));
        zumo.setOnClickListener(v -> cListener.showTextImage(v));
        fruta.setOnClickListener(v -> cListener.showTextImage(v));
        galletas.setOnClickListener(v -> cListener.showTextImage(v));
        macarrones.setOnClickListener(v -> cListener.showTextImage(v));
        yogur.setOnClickListener(v -> cListener.showTextImage(v));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ComunicationFragments) {
            cListener = (ComunicationFragments) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " Must be implement ComunicationFragments");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cListener = null;
    }
}