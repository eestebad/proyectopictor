package com.example.pictor;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class SaludFragment extends Fragment {

    private ComunicationFragments cListener;

    protected ImageButton dolorGarganta;
    protected ImageButton dolorPecho;
    protected ImageButton medicina;
    protected ImageButton centroSalud;
    protected ImageButton medico;
    protected ImageButton gelHidro;
    protected ImageButton mascarilla;
    protected ImageButton tomarTemperatura;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public SaludFragment() {
        // Required empty public constructor
    }

    public static SaludFragment newInstance(String param1, String param2) {
        SaludFragment fragment = new SaludFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_salud, container, false);

        //Initialize the ImageButtons.
        dolorGarganta = view.findViewById(R.id.imageBtnDolorGarganta);
        dolorPecho = view.findViewById(R.id.imageBtnDolorPecho);
        medicina = view.findViewById(R.id.imageBtnMedicina);
        centroSalud = view.findViewById(R.id.imageBtnCentroSalud);
        medico = view.findViewById(R.id.imageBtnMedico);
        gelHidro = view.findViewById(R.id.imageBtnGelHidro);
        mascarilla = view.findViewById(R.id.imageBtnMascarilla);
        tomarTemperatura = view.findViewById(R.id.imageBtnTomarTemperatura);

        //Initialize the ArrayList of ImageButtons.
        List<ImageButton> imageBtnSaludList = new ArrayList<>();
        imageBtnSaludList.add(dolorGarganta);
        imageBtnSaludList.add(dolorPecho);
        imageBtnSaludList.add(medicina);
        imageBtnSaludList.add(centroSalud);
        imageBtnSaludList.add(medico);
        imageBtnSaludList.add(gelHidro);
        imageBtnSaludList.add(mascarilla);
        imageBtnSaludList.add(tomarTemperatura);

        //Initialize the ArrayList of Salud Pictograms.
        List<Pictogram> pictogramSaludList = new ArrayList<>();
        pictogramSaludList.add(new Pictogram(0, "dolor de garganta ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fdolor%20de%20garganta.png?alt=media&token=158c7c40-4ae7-497c-ae05-362a76cb5654"));
        pictogramSaludList.add(new Pictogram(1, "dolor de pecho ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fdolor%20de%20pecho.png?alt=media&token=2e9b50ef-7982-4a9c-83f2-25b70d9cd291"));
        pictogramSaludList.add(new Pictogram(2, "medicina ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fmedicina.png?alt=media&token=4e0bc8bb-0c9d-424b-b3cc-6f35529d4f80"));
        pictogramSaludList.add(new Pictogram(3, "centro de salud ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fcentro%20de%20salud.png?alt=media&token=112bd888-bc31-4c44-9f22-17d3785ce7f5"));
        pictogramSaludList.add(new Pictogram(0, "médico ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fm%C3%A9dico.png?alt=media&token=564a9133-32f0-4ebe-b4d5-75bd0c94ded5"));
        pictogramSaludList.add(new Pictogram(1, "gel hidroalcohólico ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fgel%20hidroalcoh%C3%B3lico.png?alt=media&token=aa05572f-8b7e-4078-b773-130602e9cf6b"));
        pictogramSaludList.add(new Pictogram(2, "mascarilla ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fmascarilla.png?alt=media&token=d382692b-3806-4ca0-bd05-3a6fed7978b6"));
        pictogramSaludList.add(new Pictogram(3, "tomar la temperatura ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Ftomar%20la%20temperatura.png?alt=media&token=d1c4d1a6-331b-4fc0-a6f5-d51b614c3d7d"));


        int count = 0;
        //Create each ImageButton with a Pictogram.
        for (ImageButton imageButton:imageBtnSaludList
        ) {
            Glide.with(this)
                    .load(pictogramSaludList.get(count).getUrlStorage())
                    .into(imageButton);
            count++;
        }

        //Call to the method to display the Pictogram's name
        dolorGarganta.setOnClickListener(v -> cListener.showTextImage(v));
        dolorPecho.setOnClickListener(v -> cListener.showTextImage(v));
        medicina.setOnClickListener(v -> cListener.showTextImage(v));
        centroSalud.setOnClickListener(v -> cListener.showTextImage(v));
        medico.setOnClickListener(v -> cListener.showTextImage(v));
        gelHidro.setOnClickListener(v -> cListener.showTextImage(v));
        mascarilla.setOnClickListener(v -> cListener.showTextImage(v));
        tomarTemperatura.setOnClickListener(v -> cListener.showTextImage(v));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ComunicationFragments) {
            cListener = (ComunicationFragments) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " Must be implement ComunicationFragments");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cListener = null;
    }
}