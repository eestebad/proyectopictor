package com.example.pictor;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class Translator extends AppCompatActivity implements ComunicationFragments {

    protected FirebaseStorage storage = FirebaseStorage.getInstance();
    // Referencia a Cloud Storage
    protected StorageReference storageRef = storage.getReference();
     //Reference to child path.
    //StorageReference storageRefChildT = storageRef.child("acciones1.png");
    protected StorageReference imageRef;

    // ImageButtons to show the images.
    private ImageButton acciones;
    private ImageButton aseo;
    private ImageButton comida;
    private ImageButton salud;
    private ImageButton sensaciones;
    private ImageButton propios;
    private ImageButton borrar;
    private ImageButton imageButton2;
    private ImageButton imageButton3;
    private ImageButton imageButton5;
    private ImageButton imageButton6;

    List<String> listNamePicto = new ArrayList<>();
    List<ImageButton> listImgButton = new ArrayList<>();
    List<String> listUrlPictogram = new ArrayList<>();

    // Initialize the fragments.
    public AccionesFragment accionesFragment = new AccionesFragment();
    public AseoFragment aseoFragment = new AseoFragment();
    public ComidaFragment comidaFragment = new ComidaFragment();
    public SaludFragment saludFragment = new SaludFragment();
    public SensacionesFragment sensacionesFragment = new SensacionesFragment();
    public MisPictogramasFragment misPictogramasFragment = new MisPictogramasFragment();

    public TextView viewTextImage;

    public String imageName = "";
    Pictogram pQuiero = new Pictogram(1, "quiero ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fquiero.png?alt=media&token=56203412-1cfb-4295-9dad-9443a06a3f2a");
    Pictogram pNo = new Pictogram(2, "no ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fno.png?alt=media&token=457d8709-e63e-4fe0-8550-d0450f0b643a");
    Pictogram pDuchar = new Pictogram(3, "duchar ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fduchar.png?alt=media&token=4fd8b967-b8eb-45e6-abd9-a69b7cf751ac");
    Pictogram pOrinar = new Pictogram(4, "hacer pis ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Ftener%20ganas%20de%20hacer%20pis.png?alt=media&token=6158e3c5-1112-49d6-9bd5-de85b3f842d3");
    Pictogram pAgua = new Pictogram(5, "agua ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fagua.png?alt=media&token=cadfedfc-5552-41b7-a48f-2650819f9c49");
    Pictogram pCafe = new Pictogram(6, "café ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fcafe_leche.png?alt=media&token=225a510c-7835-4cd0-b6d1-8448e924c816");
    Pictogram pDolorGarganta = new Pictogram(7, "dolor de garganta ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fdolor%20de%20garganta.png?alt=media&token=158c7c40-4ae7-497c-ae05-362a76cb5654");
    Pictogram pMedico = new Pictogram(8, "médico ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fm%C3%A9dico.png?alt=media&token=564a9133-32f0-4ebe-b4d5-75bd0c94ded5");
    Pictogram pTenerCalor = new Pictogram(9, "tengo calor ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftener%20calor.png?alt=media&token=a1b2d396-0146-47a0-9285-b565772fb503");
    Pictogram pTenerSed = new Pictogram(1, "tengo sed ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftengosed.png?alt=media&token=0e99ced2-4f2a-47dd-93a1-6da4873aae15");
    Pictogram pDormir = new Pictogram(10, "dormir ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fdormir.png?alt=media&token=28cc5716-8e99-4b22-9bd3-736d5115b3ac");
    Pictogram pLeer = new Pictogram(11, "leer ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fleer.png?alt=media&token=28858dda-6cb5-421a-92d8-f5d6887b4784");
    Pictogram pLavarManos = new Pictogram(12, "lavar las manos ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2FlavarManos.png?alt=media&token=cfac926d-b180-46fb-b065-d8d4776c1321");
    Pictogram pLavarDientes = new Pictogram(13, "lavar los dientes ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fcepillar%20los%20dientes.png?alt=media&token=430328aa-b14b-4107-8406-8eebbccf7d1f");
    Pictogram pLeche = new Pictogram(14, "leche ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fleche.png?alt=media&token=fdfa86e2-4d2f-4bbe-972f-d252572df5ab");
    Pictogram pZumo = new Pictogram(15, "zumo ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fzumo.png?alt=media&token=34a5cb47-17f2-4cb1-bc0f-5d5c80e78c9c");
    Pictogram pGelHidro = new Pictogram(16, "gel hidroalcohólico ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fgel%20hidroalcoh%C3%B3lico.png?alt=media&token=aa05572f-8b7e-4078-b773-130602e9cf6b");
    Pictogram pMascarilla = new Pictogram(17, "mascarilla ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fmascarilla.png?alt=media&token=d382692b-3806-4ca0-bd05-3a6fed7978b6");
    Pictogram pCansado = new Pictogram(18, "estoy cansado ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Fcansado.png?alt=media&token=f305ac6c-ec23-4c0a-87d6-f5e43cb92a9e");
    Pictogram pTenerFrio = new Pictogram(19, "tengo frío ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftengofrio.png?alt=media&token=b1874850-e642-4671-8e4d-5695771290d4");
    Pictogram pLlamar = new Pictogram(20, "llamar ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fllamar.png?alt=media&token=f74c1910-1630-48c2-baf3-64c4e82e6db7");
    Pictogram pLlamar112 = new Pictogram(21, "llamar al 112 ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fllamar%20al%20112.png?alt=media&token=8752da55-9c22-4851-9445-0f1ce9e34803");
    Pictogram pNoEntiendo = new Pictogram(22, "no entiendo ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fno%20entiendo.png?alt=media&token=b5770dec-73d4-435a-9912-a6c1a46477ed");
    Pictogram pComer = new Pictogram(23, "comer ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fcomer.png?alt=media&token=85845fe4-7b1f-4c1f-a52e-64257cfa6a93");
    Pictogram pCuartoAseo = new Pictogram(24, "cuarto de aseo ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fcuartos%20de%20aseo.png?alt=media&token=95ab5550-1e68-43a6-9153-1ed33c11d50c");
    Pictogram pPeluqueria = new Pictogram(25, "peluquería ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fpeluquer%C3%ADa.png?alt=media&token=8e3fd0d5-f9d1-431d-8628-a6e7e779a6aa");
    Pictogram pPeinar = new Pictogram(26, "peinar ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fpeinar.png?alt=media&token=ffcdacb4-3512-4173-a6e8-00739dcf14a6");
    Pictogram pLavarCara = new Pictogram(27, "lavar la cara ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2FlavarCara.png?alt=media&token=23b94c78-7606-4de2-a0c8-67d7500e3d27");
    Pictogram pFruta = new Pictogram(28, "fruta ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Ffruta.png?alt=media&token=c6f08e00-f1aa-4c6b-9ee5-20531e1f046a");
    Pictogram pGalletas = new Pictogram(29, "galletas ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fgalletas.png?alt=media&token=179d4c9b-5bea-4eb2-b971-8e4fbbb0cd9f");
    Pictogram pMacarrones = new Pictogram(30, "macarrones ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fmacarrones.png?alt=media&token=62611f67-1640-4823-8f1d-3af05d9abd68");
    Pictogram pYogur = new Pictogram(31, "yogur ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida%2Fyogur.png?alt=media&token=0dd0c532-388e-4d53-8e88-662c86c35361");
    Pictogram pTenerHambre = new Pictogram(32, "tengo hambre ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftener%20hambre.png?alt=media&token=e83a6ce4-22fa-4c3e-a414-d4e38fe828ee");
    Pictogram pEnfermo = new Pictogram(33, "me encuentro mal ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Fenfermo.png?alt=media&token=749fb263-81bd-4a4c-9888-d9ae161062c1");
    Pictogram pLoSiento = new Pictogram(34, "lo siento ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Flo%20siento.png?alt=media&token=02f9728a-d91b-4e0e-bcc2-e7626bafc900");
    Pictogram pMeGusta = new Pictogram(35, "me gusta ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Fgustar.png?alt=media&token=ea28f62e-24a5-4fa3-8220-4a45787288ee");
    Pictogram pDolorPecho = new Pictogram(36, "me duele el pecho ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fdolor%20de%20pecho.png?alt=media&token=2e9b50ef-7982-4a9c-83f2-25b70d9cd291");
    Pictogram pMedicina = new Pictogram(37, "medicina ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fmedicina.png?alt=media&token=4e0bc8bb-0c9d-424b-b3cc-6f35529d4f80");
    Pictogram pCentroSalud = new Pictogram(38, "centro de salud ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Fcentro%20de%20salud.png?alt=media&token=112bd888-bc31-4c44-9f22-17d3785ce7f5");
    Pictogram pTomarTemperatura = new Pictogram(39, "tomar la temperatura ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud%2Ftomar%20la%20temperatura.png?alt=media&token=d1c4d1a6-331b-4fc0-a6f5-d51b614c3d7d");

    public int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translator);

        acciones = (ImageButton) findViewById(R.id.imageBtnAcciones);
        aseo = (ImageButton) findViewById(R.id.imageBtnAseo);
        comida = (ImageButton) findViewById(R.id.imageBtnComida);
        salud = (ImageButton) findViewById(R.id.imageBtnSalud);
        sensaciones = (ImageButton) findViewById(R.id.imageBtnSensaciones);
        propios = (ImageButton) findViewById(R.id.imageBtnPropios);
        borrar = (ImageButton)findViewById(R.id.imgBorrar);
        imageButton2 = (ImageButton) findViewById(R.id.imageButton2);
        imageButton3 = (ImageButton) findViewById(R.id.imageButton3);
        imageButton5 = (ImageButton) findViewById(R.id.imageButton5);
        imageButton6 = (ImageButton) findViewById(R.id.imageButton6);

        listImgButton.add(acciones);
        listImgButton.add(aseo);
        listImgButton.add(comida);
        listImgButton.add(salud);
        listImgButton.add(sensaciones);
        listImgButton.add(propios);
        listImgButton.add(borrar);

        listNamePicto.add("acciones1.png");
        listNamePicto.add("aseo.png");
        listNamePicto.add("comida.png");
        listNamePicto.add("salud.png");
        listNamePicto.add("sensaciones.png");
        listNamePicto.add("mispictogramas.png");
        listNamePicto.add("borrar.png");

        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones1.png?alt=media&token=01c7f550-a1e8-430f-966d-f980453cf9ab");
        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo.png?alt=media&token=81d91cc8-1b14-4ff4-9586-2386b12538ce");
        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/comida.png?alt=media&token=7098582f-1a4b-4ccd-8496-8faa458a976e");
        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/salud.png?alt=media&token=7b04fd40-b486-412a-9a9a-f19c0f3d692d");
        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones.png?alt=media&token=e4d152cb-c124-4d2c-91ac-2c42c1520fdf");
        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/mispictogramas.png?alt=media&token=a48b84a8-a1b7-4732-9a72-47e90fcdc8b9");
        listUrlPictogram.add("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/borrar.png?alt=media&token=845ccb2c-d5a7-4ecb-9ce3-908ad2da8377");

        List<Pictogram> pictogramTranslatorList = new ArrayList<>();
        int contador = 0;

        for (String lString:listNamePicto)
        {
            StorageReference storageRefChildT = storageRef.child("lString");
            String uriPicto = storageRefChildT.getDownloadUrl().toString();

            pictogramTranslatorList.add(new Pictogram(contador, lString, listUrlPictogram.get(contador).toString()));
            contador++;
        }

        int count = 0;
        for (ImageButton lImageButton:listImgButton) {

            try {
                Glide.with(this)
                        .load(pictogramTranslatorList.get(count++).getUrlStorage())
                        .into(lImageButton);
            } catch (Exception e) {
                Toast.makeText(Translator.this, "Fallo en la aplicación ", Toast.LENGTH_SHORT).show();
            }
        }

        //Collect the xml textView:
        viewTextImage = findViewById(R.id.textViewMuestraTexto);
    }

    //Create menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.translator_menu, menu);
        return true;
    }

    // Handle item selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.translatormenu:
                Toast.makeText(this, "Autor pictogramas: Sergio Palao\nOrigen: ARASAAC (http://www.arasaac.org)\nLicencia: CC (BY-NC-SA)\nPropiedad: Gobierno de Aragón (España)", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showTextImage(View view) {
        final int idQuiero = R.id.imageBtnQuiero;
        final int idNo = R.id.imageBtnNo;
        final int idDuchar = R.id.imageBtnDuchar;
        final int idOrinar = R.id.imageBtnOrinar;
        final int idAgua = R.id.imageBtnAgua;
        final int idCafe = R.id.imageBtnCafe;
        final int idDolorGarganta = R.id.imageBtnDolorGarganta;
        final int idMedico = R.id.imageBtnMedico;
        final int idTenerCalor = R.id.imageBtnTenerCalor;
        final int idTenerSed = R.id.imageBtnTenerSed;
        final int idDormir = R.id.imageBtnDormir;
        final int idLeer = R.id.imageBtnLeer;
        final int idLavarManos = R.id.imageBtnLavarManos;
        final int idLavarDientes = R.id.imageBtnDientes;
        final int idLeche = R.id.imageBtnLeche;
        final int idZumo = R.id.imageBtnZumo;
        final int idGelHidro = R.id.imageBtnGelHidro;
        final int idMascarilla = R.id.imageBtnMascarilla;
        final int idCansado = R.id.imageBtnEstoyCansado;
        final int idTenerFrio = R.id.imageBtnTenerFrio;
        final int idLlamar = R.id.imageBtnLlamar;
        final int idLlamar112 = R.id.imageBtnLlamar112;
        final int idNoEntiendo = R.id.imageBtnNoEntiendo;
        final int idComer = R.id.imageBtnComer;
        final int idCuartoAseo = R.id.imageBtnCuartoAseo;
        final int idPeluqueria = R.id.imageBtnPeluqueria;
        final int idPeinar = R.id.imageBtnPeinar;
        final int idLavarCara = R.id.imageBtnLavarCara;
        final int idFruta = R.id.imageBtnFruta;
        final int idGalletas = R.id.imageBtnGalletas;
        final int idMacarrones = R.id.imageBtnMacarrones;
        final int idYogur = R.id.imageBtnYogur;
        final int idTenerHambre = R.id.imageBtnTenerHambre;
        final int idEnfermo = R.id.imageBtnEstoyEnfermo;
        final int idLoSiento = R.id.imageBtnLoSiento;
        final int idMeGusta = R.id.imageBtnMeGusta;
        final int idDolorPecho = R.id.imageBtnDolorPecho;
        final int idMedicina = R.id.imageBtnMedicina;
        final int idCentroSalud = R.id.imageBtnCentroSalud;
        final int idTomarTemperatura = R.id.imageBtnTomarTemperatura;

        switch (view.getId())
        {
            case idQuiero:
                show(contador, pQuiero);
                contador++;
                break;
            case idNo:
                show(contador, pNo);
                contador++;
                break;
            case idDuchar:
                show(contador, pDuchar);
                contador++;
                break;
            case idOrinar:
                show(contador, pOrinar);
                contador++;
                break;
            case idAgua:
                show(contador, pAgua);
                contador++;
                break;
            case idCafe:
                show(contador, pCafe);
                contador++;
                break;
            case idDolorGarganta:
                show(contador, pDolorGarganta);
                contador++;
                break;
            case idMedico:
                show(contador, pMedico);
                contador++;
                break;
            case idTenerCalor:
                show(contador, pTenerCalor);
                contador++;
                break;
            case idTenerSed:
                show(contador, pTenerSed);
                contador++;
                break;
            case idDormir:
                show(contador, pDormir);
                contador++;
                break;
            case idLeer:
                show(contador, pLeer);
                contador++;
                break;
            case idLavarManos:
                show(contador, pLavarManos);
                contador++;
                break;
            case idLavarDientes:
                show(contador, pLavarDientes);
                contador++;
                break;
            case idLeche:
                show(contador, pLeche);
                contador++;
                break;
            case idZumo:
                show(contador, pZumo);
                contador++;
                break;
            case idGelHidro:
                show(contador, pGelHidro);
                contador++;
                break;
            case idMascarilla:
                show(contador, pMascarilla);
                contador++;
                break;
            case idCansado:
                show(contador, pCansado);
                contador++;
                break;
            case idTenerFrio:
                show(contador, pTenerFrio);
                contador++;
                break;
            case idLlamar:
                show(contador, pLlamar);
                contador++;
                break;
            case idLlamar112:
                show(contador, pLlamar112);
                contador++;
                break;
            case idNoEntiendo:
                show(contador, pNoEntiendo);
                contador++;
                break;
            case idComer:
                show(contador, pComer);
                contador++;
                break;
            case idCuartoAseo:
                show(contador, pCuartoAseo);
                contador++;
                break;
            case idPeluqueria:
                show(contador, pPeluqueria);
                contador++;
                break;
            case idPeinar:
                show(contador, pPeinar);
                contador++;
                break;
            case idLavarCara:
                show(contador, pLavarCara);
                contador++;
                break;
            case idFruta:
                show(contador, pFruta);
                contador++;
                break;
            case idGalletas:
                show(contador, pGalletas);
                contador++;
                break;
            case idMacarrones:
                show(contador, pMacarrones);
                contador++;
                break;
            case idYogur:
                show(contador, pYogur);
                contador++;
                break;
            case idTenerHambre:
                show(contador, pTenerHambre);
                contador++;
                break;
            case idEnfermo:
                show(contador, pEnfermo);
                contador++;
                break;
            case idLoSiento:
                show(contador, pLoSiento);
                contador++;
                break;
            case idMeGusta:
                show(contador, pMeGusta);
                contador++;
                break;
            case idDolorPecho:
                show(contador, pDolorPecho);
                contador++;
                break;
            case idMedicina:
                show(contador, pMedicina);
                contador++;
                break;
            case idCentroSalud:
                show(contador, pCentroSalud);
                contador++;
                break;
            case idTomarTemperatura:
                show(contador, pTomarTemperatura);
                contador++;
                break;
            default:
                break;
        }
    }

    public void deleteTextView(View view) {
        imageName = "";
        //Delete the text.
        viewTextImage.setText(imageName);
        //Delete the pictogram of the ImageButtons.
        try {
            Glide.with(this)
                    .load("")
                    .into(imageButton2);
            Glide.with(this)
                    .load("")
                    .into(imageButton3);
            Glide.with(this)
                    .load("")
                    .into(imageButton5);
            Glide.with(this)
                    .load("")
                    .into(imageButton6);
        } catch (Exception e) {
            Toast.makeText(Translator.this, "Fallo en la aplicación ", Toast.LENGTH_SHORT).show();
        }
        contador = 0;
    }

    @Override
    public UserInformation recoverUser(UserInformation userInformation) {
        return userInformation;
    }


    //Change the fragment.
    public void initFragment(View view) {
        //Incluir el contenedor para mostrar los fragmentos.
        switch (view.getId())
        {
            case R.id.imageBtnAcciones:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerTranslator,accionesFragment)
                        .commit();
                break;
            case R.id.imageBtnAseo:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerTranslator,aseoFragment)
                        .commit();
                break;
            case R.id.imageBtnComida:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerTranslator,comidaFragment)
                        .commit();
                break;
            case R.id.imageBtnSalud:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerTranslator,saludFragment)
                        .commit();
                break;
            case R.id.imageBtnSensaciones:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerTranslator,sensacionesFragment)
                        .commit();
                break;
            case R.id.imageBtnPropios:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerTranslator,misPictogramasFragment)
                        .commit();
                break;
            default:
                break;
        }
    }
    public void Acercade(View view){
            Toast.makeText(this, "Autor pictogramas: Sergio Palao\nOrigen: ARASAAC (http://www.arasaac.org)\nLicencia: CC (BY-NC-SA)\nPropiedad: Gobierno de Aragón (España)\nDesarrolladores : Elena Esteban y Mario Valle", Toast.LENGTH_LONG).show();
    }

    public void show(int contador, Pictogram pictogram)
    {
        if(contador == 0)
        {
            imageRef = storageRef.child(pictogram.getPictoName());
            imageName += imageRef.getName();
            viewTextImage.setText(imageName);
             try {
                Glide.with(this)
                        .load(pictogram.getUrlStorage())
                        .into(imageButton2);
            } catch (Exception e) {
                Toast.makeText(Translator.this, "Fallo en la aplicación ", Toast.LENGTH_SHORT).show();
            }
        }
        if(contador == 1)
        {
            imageRef = storageRef.child(pictogram.getPictoName());
            imageName += imageRef.getName();
            viewTextImage.setText(imageName);
            try {
                Glide.with(this)
                        .load(pictogram.getUrlStorage())
                        .into(imageButton3);
            } catch (Exception e) {
                Toast.makeText(Translator.this, "Fallo en la aplicación ", Toast.LENGTH_SHORT).show();
            }
        }
        if(contador == 2)
        {
            imageRef = storageRef.child(pictogram.getPictoName());
            imageName += imageRef.getName();
            viewTextImage.setText(imageName);
            try {
                Glide.with(this)
                        .load(pictogram.getUrlStorage())
                        .into(imageButton5);
            } catch (Exception e) {
                Toast.makeText(Translator.this, "Fallo en la aplicación ", Toast.LENGTH_SHORT).show();
            }
        }
        if(contador == 3)
        {
            imageRef = storageRef.child(pictogram.getPictoName());
            imageName += imageRef.getName();
            viewTextImage.setText(imageName);
            try {
                Glide.with(this)
                        .load(pictogram.getUrlStorage())
                        .into(imageButton6);
            } catch (Exception e) {
                Toast.makeText(Translator.this, "Fallo en la aplicación ", Toast.LENGTH_SHORT).show();
            }
        }
    }
}


