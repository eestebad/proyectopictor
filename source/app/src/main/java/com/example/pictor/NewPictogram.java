package com.example.pictor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NewPictogram extends AppCompatActivity{

    EditText namePictogram;
    ImageButton imgBtnShootPhoto;
    ImageView imageView;
    String imagePath;
    String nameP;
    String uriNewPictogram;
    public String nombrePicto = "nameNew";

    boolean stop = true;

    FirebaseStorage storageConection = FirebaseStorage.getInstance(); //Conection to Cloud Storage.
    StorageReference storageRefRoot = storageConection.getReference(); //Reference to parent path.
    StorageReference storageRefChild = storageRefRoot.child("personalized"); //Reference to child path.
    //Reference to Authentication.
    FirebaseAuth firebaseAuth;
    FirebaseUser actualUserFirebase;

    //Reference RealTime DataBase.
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference realTimeDatabaseref = database.getReference();

    public UploadTask uploadTask;

    Uri UriPhoto;

    UserInformation actualUser = new UserInformation();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pictogram);
        //Ask for write external storage permission.
        if (ContextCompat.checkSelfPermission(NewPictogram.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NewPictogram.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NewPictogram.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1000);
        }

        imgBtnShootPhoto = findViewById(R.id.imageBtnShootPhoto);
        imageView = findViewById(R.id.imageViewPhoto);
        namePictogram = (EditText)findViewById(R.id.editTextTextPictogram);

        imgBtnShootPhoto.setOnClickListener(v -> shootPictogram());
    }

    //Method for shoot a photo using the device camera.
    public void shootPictogram(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            //name = namePictogram.getText().toString();
            File fileImage = null;

            try {
                fileImage = savePictogram();
            }catch (IOException ex){
                Log.e("Error", ex.toString());
            }

            if(fileImage != null){
                UriPhoto = FileProvider.getUriForFile(this, "com.example.pictor.fileprovider", fileImage);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, UriPhoto);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }

    //Method to show the photo in the imageView.
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK) {
            //actualUser = (UserInformation)getIntent().getSerializableExtra("user");
            Bitmap imgBitmap = BitmapFactory.decodeFile(imagePath);
            imageView.setImageBitmap(imgBitmap);

            imageView.setDrawingCacheEnabled(true);
            imageView.buildDrawingCache();
            Bitmap bitMap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitMap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] dataB = baos.toByteArray();

            nameP = namePictogram.getText().toString();
            StorageReference newPictogram = storageRefChild.child(String.valueOf(nameP)); //Reference to upload the new pictogram.

            //Load the photo in Reference of Cloud Storage.
            uploadTask = newPictogram.putBytes(dataB);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(NewPictogram.this, "No se ha podido guardar la foto.", Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(taskSnapshot -> Toast.makeText(NewPictogram.this, "Se ha guardado la foto.", Toast.LENGTH_SHORT).show());

            //Get down load url.
            uploadTask = newPictogram.putFile(UriPhoto);

            Task<Uri> urlTask = uploadTask.continueWithTask((Continuation<UploadTask.TaskSnapshot, Task<Uri>>) task -> {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return newPictogram.getDownloadUrl();
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        uriNewPictogram = downloadUri.toString();

                        realTimeDatabaseref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                try {
                                    if (stop) //Prevent repeating "if (snapshot.exists())" indefinitely
                                    {
                                        if (snapshot.exists()) {
                                            if(task.isSuccessful()) {
                                                Map<String, Object> map = new HashMap<>();
                                                firebaseAuth = FirebaseAuth.getInstance();
                                                String id = firebaseAuth.getCurrentUser().getUid();

                                                if (id != null && !id.isEmpty()) {
                                                    //Search actual user data.
                                                    for (DataSnapshot ds : snapshot.getChildren()) {
                                                        ds.getValue();
                                                        actualUser.setNombrePicto1(ds.child(id).getValue(UserInformation.class).getNombrePicto1());
                                                        actualUser.setNombrePicto2(ds.child(id).getValue(UserInformation.class).getNombrePicto2());
                                                        actualUser.setNombrePicto3(ds.child(id).getValue(UserInformation.class).getNombrePicto3());
                                                        actualUser.setNombrePicto4(ds.child(id).getValue(UserInformation.class).getNombrePicto4());
                                                        actualUser.setNombrePicto5(ds.child(id).getValue(UserInformation.class).getNombrePicto5());
                                                        actualUser.setNombrePicto6(ds.child(id).getValue(UserInformation.class).getNombrePicto6());
                                                        actualUser.setNombrePicto7(ds.child(id).getValue(UserInformation.class).getNombrePicto7());
                                                        actualUser.setNombrePicto8(ds.child(id).getValue(UserInformation.class).getNombrePicto8());
                                                    }

                                                    if (actualUser.getNombrePicto1().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto1", nameP);
                                                        map.put("urlPicto1", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto2().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto2", nameP);
                                                        map.put("urlPicto2", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto2().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto3().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto3", nameP);
                                                        map.put("urlPicto3", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto2().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto3().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto4().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto4", nameP);
                                                        map.put("urlPicto4", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto2().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto3().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto4().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto5().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto5", nameP);
                                                        map.put("urlPicto5", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto2().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto3().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto4().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto5().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto6().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto6", nameP);
                                                        map.put("urlPicto6", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto2().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto3().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto4().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto5().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto6().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto7().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto7", nameP);
                                                        map.put("urlPicto7", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                    if (!actualUser.getNombrePicto1().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto2().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto3().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto4().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto5().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto6().equals(nombrePicto) &&
                                                            !actualUser.getNombrePicto7().equals(nombrePicto) &&
                                                            actualUser.getNombrePicto8().equals(nombrePicto)) {
                                                        //Put the name and the uri of the new pictogram in the map.
                                                        map.put("nombrePicto8", nameP);
                                                        map.put("urlPicto8", uriNewPictogram);
                                                        //Update the child.
                                                        realTimeDatabaseref.child("People").child(id).updateChildren(map);
                                                        stop = false;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }catch(Exception e)
                                {
                                    Toast.makeText(NewPictogram.this, "No se ha guardado correctamente.", Toast.LENGTH_SHORT).show();
                                }


                            }
                            @Override
                            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                                Toast.makeText(NewPictogram.this, "No se ha guardado correctamente.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(NewPictogram.this, "No se ha guardado correctamente.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    //Save image.
    private File savePictogram() throws IOException {
        String name = namePictogram.getText().toString();
        File path = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File filePictogram = File.createTempFile(name, ".jpg", path);

        imagePath = filePictogram.getAbsolutePath();

        return filePictogram;
    }
}