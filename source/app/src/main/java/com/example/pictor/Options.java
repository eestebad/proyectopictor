package com.example.pictor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Options extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

    }

    public void initTranslator(View view) {
        //Declaración del Intent explícito para ir a la ventana Traductor:
        Intent intentTranslator = new Intent(this, Translator.class);
        //Ejecución del Intent:
        startActivity(intentTranslator);
    }

    public void initNewPictogram(View view) {
        //Declaración del Intent explícito para ir a la ventana Nuevo Pictograma:
        Intent intentNewPictogram= new Intent(this, NewPictogram.class);
        //Ejecución del Intent:
        startActivity(intentNewPictogram);
    }

    public void initRecordVideo(View view) {
        //Declaración del Intent explícito para ir a la ventana Grabar vídeo:
        Intent intentRecordVideo= new Intent(this, RecordVideo.class);
        //Ejecución del Intent:
        startActivity(intentRecordVideo);
    }

    public void initViewVideo(View view) {
        //Declaración del Intent explícito para ir a la ventana Nuevo Vídeo:
        Intent intentNewVideo= new Intent(this, ViewVideo.class);
        //Ejecución del Intent:
        startActivity(intentNewVideo);
    }
}