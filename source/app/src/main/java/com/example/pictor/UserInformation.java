package com.example.pictor;

import java.io.Serializable;

public class UserInformation implements Serializable {
    private String email;
    private String password;
    private String Rol;
    private String Rela;
    private String nombrePicto1;
    private String urlPicto1;
    private String nombrePicto2;
    private String urlPicto2;
    private String nombrePicto3;
    private String urlPicto3;
    private String nombrePicto4;
    private String urlPicto4;
    private String nombrePicto5;
    private String urlPicto5;
    private String nombrePicto6;
    private String urlPicto6;
    private String nombrePicto7;
    private String urlPicto7;
    private String nombrePicto8;
    private String urlPicto8;


    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }
    public String getRela() {

        return Rela;
    }

    public void setRela(String Rela) {

        this.Rela = Rela;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRol() {

        return Rol;
    }

    public void setRol(String Rol) {

        this.Rol = Rol;
    }

    public String getNombrePicto1() {
        return nombrePicto1;
    }

    public void setNombrePicto1(String nombrePicto1) {
        this.nombrePicto1 = nombrePicto1;
    }

    public String getUrlPicto1() {
        return urlPicto1;
    }

    public void setUrlPicto1(String urlPicto1) {
        this.urlPicto1 = urlPicto1;
    }

    public String getNombrePicto2() {
        return nombrePicto2;
    }

    public void setNombrePicto2(String nombrePicto2) {
        this.nombrePicto2 = nombrePicto2;
    }

    public String getUrlPicto2() {
        return urlPicto2;
    }

    public void setUrlPicto2(String urlPicto2) {
        this.urlPicto2 = urlPicto2;
    }

    public String getNombrePicto3() {
        return nombrePicto3;
    }

    public void setNombrePicto3(String nombrePicto3) {
        this.nombrePicto3 = nombrePicto3;
    }

    public String getUrlPicto3() {
        return urlPicto3;
    }

    public void setUrlPicto3(String urlPicto3) {
        this.urlPicto3 = urlPicto3;
    }

    public String getNombrePicto4() {
        return nombrePicto4;
    }

    public void setNombrePicto4(String nombrePicto4) {
        this.nombrePicto4 = nombrePicto4;
    }

    public String getUrlPicto4() {
        return urlPicto4;
    }

    public void setUrlPicto4(String urlPicto4) {
        this.urlPicto4 = urlPicto4;
    }

    public String getNombrePicto5() {
        return nombrePicto5;
    }

    public void setNombrePicto5(String nombrePicto5) {
        this.nombrePicto5 = nombrePicto5;
    }

    public String getUrlPicto5() {
        return urlPicto5;
    }

    public void setUrlPicto5(String urlPicto5) {
        this.urlPicto5 = urlPicto5;
    }

    public String getNombrePicto6() {
        return nombrePicto6;
    }

    public void setNombrePicto6(String nombrePicto6) {
        this.nombrePicto6 = nombrePicto6;
    }

    public String getUrlPicto6() {
        return urlPicto6;
    }

    public void setUrlPicto6(String urlPicto6) {
        this.urlPicto6 = urlPicto6;
    }

    public String getNombrePicto7() {
        return nombrePicto7;
    }

    public void setNombrePicto7(String nombrePicto7) {
        this.nombrePicto7 = nombrePicto7;
    }

    public String getUrlPicto7() {
        return urlPicto7;
    }

    public void setUrlPicto7(String urlPicto7) {
        this.urlPicto7 = urlPicto7;
    }

    public String getNombrePicto8() {
        return nombrePicto8;
    }

    public void setNombrePicto8(String nombrePicto8) {
        this.nombrePicto8 = nombrePicto8;
    }

    public String getUrlPicto8() {
        return urlPicto8;
    }

    public void setUrlPicto8(String urlPicto8) {
        this.urlPicto8 = urlPicto8;
    }
}

