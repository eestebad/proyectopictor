package com.example.pictor;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class SensacionesFragment extends Fragment {

    private ComunicationFragments cListener;

    protected ImageButton tenerHambre;
    protected ImageButton tenerSed;
    protected ImageButton frio;
    protected ImageButton calor;
    protected ImageButton cansado;
    protected ImageButton enfermo;
    protected ImageButton loSiento;
    protected ImageButton meGusta;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public SensacionesFragment() {
        // Required empty public constructor
    }

    public static SensacionesFragment newInstance(String param1, String param2) {
        SensacionesFragment fragment = new SensacionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sensaciones, container, false);

        //Initialize the ImageButtons.
        tenerHambre = view.findViewById(R.id.imageBtnTenerHambre);
        tenerSed = view.findViewById(R.id.imageBtnTenerSed);
        frio = view.findViewById(R.id.imageBtnTenerFrio);
        calor = view.findViewById(R.id.imageBtnTenerCalor);
        cansado = view.findViewById(R.id.imageBtnEstoyCansado);
        enfermo = view.findViewById(R.id.imageBtnEstoyEnfermo);
        loSiento = view.findViewById(R.id.imageBtnLoSiento);
        meGusta = view.findViewById(R.id.imageBtnMeGusta);

        //Initialize the ArrayList of ImageButtons.
        List<ImageButton> imageBtnSensacionesList = new ArrayList<>();
        imageBtnSensacionesList.add(tenerHambre);
        imageBtnSensacionesList.add(tenerSed);
        imageBtnSensacionesList.add(frio);
        imageBtnSensacionesList.add(calor);
        imageBtnSensacionesList.add(cansado);
        imageBtnSensacionesList.add(enfermo);
        imageBtnSensacionesList.add(loSiento);
        imageBtnSensacionesList.add(meGusta);

        //Initialize the ArrayList of Salud Pictograms.
        List<Pictogram> pictogramSensacionesList = new ArrayList<>();
        pictogramSensacionesList.add(new Pictogram(0, "tengo hambre ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftener%20hambre.png?alt=media&token=e83a6ce4-22fa-4c3e-a414-d4e38fe828ee"));
        pictogramSensacionesList.add(new Pictogram(1, "tengo sed ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftengosed.png?alt=media&token=0e99ced2-4f2a-47dd-93a1-6da4873aae15"));
        pictogramSensacionesList.add(new Pictogram(2, "tengo frío ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftengofrio.png?alt=media&token=b1874850-e642-4671-8e4d-5695771290d4"));
        pictogramSensacionesList.add(new Pictogram(3, "tengo calor ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Ftener%20calor.png?alt=media&token=a1b2d396-0146-47a0-9285-b565772fb503"));
        pictogramSensacionesList.add(new Pictogram(0, "estoy cansado ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Fcansado.png?alt=media&token=f305ac6c-ec23-4c0a-87d6-f5e43cb92a9e"));
        pictogramSensacionesList.add(new Pictogram(1, "me encuentro mal ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Fenfermo.png?alt=media&token=749fb263-81bd-4a4c-9888-d9ae161062c1"));
        pictogramSensacionesList.add(new Pictogram(2, "lo siento ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Flo%20siento.png?alt=media&token=02f9728a-d91b-4e0e-bcc2-e7626bafc900"));
        pictogramSensacionesList.add(new Pictogram(3, "me gusta ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/sensaciones%2Fgustar.png?alt=media&token=ea28f62e-24a5-4fa3-8220-4a45787288ee"));

        int count = 0;
        //Create each ImageButton with a Pictogram.
        for (ImageButton imageButton:imageBtnSensacionesList
        ) {
            Glide.with(this)
                    .load(pictogramSensacionesList.get(count).getUrlStorage())
                    .into(imageButton);
            count++;
        }

        //Call to the method to display the Pictogram's name
        tenerHambre.setOnClickListener(v -> cListener.showTextImage(v));
        tenerSed.setOnClickListener(v -> cListener.showTextImage(v));
        frio.setOnClickListener(v -> cListener.showTextImage(v));
        calor.setOnClickListener(v -> cListener.showTextImage(v));
        cansado.setOnClickListener(v -> cListener.showTextImage(v));
        enfermo.setOnClickListener(v -> cListener.showTextImage(v));
        loSiento.setOnClickListener(v -> cListener.showTextImage(v));
        meGusta.setOnClickListener(v -> cListener.showTextImage(v));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ComunicationFragments) {
            cListener = (ComunicationFragments) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " Must be implement ComunicationFragments");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cListener = null;
    }
}