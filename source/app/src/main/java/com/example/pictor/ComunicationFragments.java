package com.example.pictor;

import android.view.View;

public interface ComunicationFragments {
    public void showTextImage(View view);
    public void deleteTextView(View view);
    public UserInformation recoverUser(UserInformation userInformation);
}
