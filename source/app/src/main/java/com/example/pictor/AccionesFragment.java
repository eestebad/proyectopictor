package com.example.pictor;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AccionesFragment extends Fragment {

    private ComunicationFragments cListener;

    protected ImageButton quiero;
    protected ImageButton no;
    protected ImageButton dormir;
    protected ImageButton leer;
    protected ImageButton llamar;
    protected ImageButton llamar112;
    protected ImageButton noEntiendo;
    protected ImageButton comer;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public AccionesFragment() {
        // Required empty public constructor
    }

    public static AccionesFragment newInstance(String param1, String param2) {
        AccionesFragment fragment = new AccionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_acciones, container, false);

        //Initialize the ImageButtons.
        quiero = view.findViewById(R.id.imageBtnQuiero);
        no = view.findViewById(R.id.imageBtnNo);
        dormir = view.findViewById(R.id.imageBtnDormir);
        leer = view.findViewById(R.id.imageBtnLeer);
        llamar = view.findViewById(R.id.imageBtnLlamar);
        llamar112 = view.findViewById(R.id.imageBtnLlamar112);
        noEntiendo = view.findViewById(R.id.imageBtnNoEntiendo);
        comer = view.findViewById(R.id.imageBtnComer);

        //Initialize the ArrayList of ImageButtons.
        List<ImageButton> imageBtnAccionesList = new ArrayList<>();
        imageBtnAccionesList.add(quiero);
        imageBtnAccionesList.add(no);
        imageBtnAccionesList.add(dormir);
        imageBtnAccionesList.add(leer);
        imageBtnAccionesList.add(llamar);
        imageBtnAccionesList.add(llamar112);
        imageBtnAccionesList.add(noEntiendo);
        imageBtnAccionesList.add(comer);

        //Initialize the ArrayList of Aseo Pictograms.
        List<Pictogram> pictogramAccionesList = new ArrayList<>();
        pictogramAccionesList.add(new Pictogram(0, "quiero ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fquiero.png?alt=media&token=56203412-1cfb-4295-9dad-9443a06a3f2a"));
        pictogramAccionesList.add(new Pictogram(1, "no ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fno.png?alt=media&token=457d8709-e63e-4fe0-8550-d0450f0b643a"));
        pictogramAccionesList.add(new Pictogram(2, "dormir ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fdormir.png?alt=media&token=28cc5716-8e99-4b22-9bd3-736d5115b3ac"));
        pictogramAccionesList.add(new Pictogram(3, "leer ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fleer.png?alt=media&token=28858dda-6cb5-421a-92d8-f5d6887b4784"));
        pictogramAccionesList.add(new Pictogram(4, "llamar ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fllamar.png?alt=media&token=f74c1910-1630-48c2-baf3-64c4e82e6db7"));
        pictogramAccionesList.add(new Pictogram(5, "llamar al 112 ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fllamar%20al%20112.png?alt=media&token=8752da55-9c22-4851-9445-0f1ce9e34803"));
        pictogramAccionesList.add(new Pictogram(6, "no entiendo ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fno%20entiendo.png?alt=media&token=b5770dec-73d4-435a-9912-a6c1a46477ed"));
        pictogramAccionesList.add(new Pictogram(7, "comer ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/acciones%2Fcomer.png?alt=media&token=85845fe4-7b1f-4c1f-a52e-64257cfa6a93"));

        int count = 0;

        //Create each ImageButton with a Pictogram.
        for (ImageButton imageButton:imageBtnAccionesList
        ) {
            Glide.with(this)
                    .load(pictogramAccionesList.get(count).getUrlStorage())
                    .into(imageButton);
            count++;
        }

        //Call to the method to display the Pictogram's name
        quiero.setOnClickListener(v -> cListener.showTextImage(v));
        no.setOnClickListener(v -> cListener.showTextImage(v));
        dormir.setOnClickListener(v -> cListener.showTextImage(v));
        leer.setOnClickListener(v -> cListener.showTextImage(v));
        llamar.setOnClickListener(v -> cListener.showTextImage(v));
        llamar112.setOnClickListener(v -> cListener.showTextImage(v));
        noEntiendo.setOnClickListener(v -> cListener.showTextImage(v));
        comer.setOnClickListener(v -> cListener.showTextImage(v));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ComunicationFragments) {
            cListener = (ComunicationFragments) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " Must be implement ComunicationFragments");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cListener = null;
    }
}
