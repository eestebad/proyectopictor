package com.example.pictor;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class ViewVideo extends AppCompatActivity {

    ImageButton AbrirVideo;
    VideoView Video;
    private static final int REQUEST_PERMISSION_CODE = 100;
    private static final int REQUEST_VIDEO_GALLERY = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_video);

        AbrirVideo= findViewById(R.id.imageButton);
        Video= findViewById(R.id.videoView);
        AbrirVideo.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ActivityCompat.checkSelfPermission(ViewVideo.this ,Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED){
                AbrirGaleria();//llamar a la funcion para que la ejecute

            }else{
                ActivityCompat.requestPermissions(ViewVideo.this ,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_PERMISSION_CODE);
            }
        }else{
            AbrirGaleria();
        }
    }
});

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {//seleccion de el video y el start para iniciarlo en el videoview llamado video
        if (requestCode == REQUEST_VIDEO_GALLERY) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Uri video = data.getData();
                Video.setVideoURI(video);
                Video.start();

            } else {
                Toast.makeText(this, "No has cogido ningun video de la galeria, prueba de nuevo.", Toast.LENGTH_SHORT).show();
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){//para verificar si estan activados los permisos
        if(requestCode == REQUEST_PERMISSION_CODE){
            if(permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                AbrirGaleria();
            }else{
                Toast.makeText(this,"Tienes que dejar los permisos, si no, no puedes acceder a por el vídeo", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }
    private void AbrirGaleria(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");//para solo mostrar los videos
        startActivityForResult(intent,REQUEST_VIDEO_GALLERY);//sacar el resultado de lo seleccionarlo y reporducirlo
    }
}