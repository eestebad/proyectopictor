package com.example.pictor;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;


public class MisPictogramasFragment extends Fragment {

    private ComunicationFragments cListener;
    private FirebaseAuth firebaseAuth;
    public UserInformation uInfo = new UserInformation();
    public String nombrePicto1;
    public String urlPicto1;
    public String nombrePicto2;
    public String urlPicto2;
    public String nombrePicto3;
    public String urlPicto3;
    public String nombrePicto4;
    public String urlPicto4;
    public String nombrePicto5;
    public String urlPicto5;
    public String nombrePicto6;
    public String urlPicto6;
    public String nombrePicto7;
    public String urlPicto7;
    public String nombrePicto8;
    public String urlPicto8;
    public String UserID;
    public String Rela;
    public String Relacion1;
    public String Rol;
    public String Emailusers;
    public String nombrePicto = "nameNew";
    FirebaseStorage storageConection = FirebaseStorage.getInstance(); //Conection to Cloud Storage.
    StorageReference storageRefRoot = storageConection.getReference(); //Reference to parent path.
    StorageReference storageRefChildT = storageRefRoot.child("personalized/adios.jpeg");
    String uriPicto = storageRefChildT.getDownloadUrl().toString();

    //Reference RealTime DataBase.
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference realTimeDatabaseref = database.getReference();

    Task<Uri> upLoadTaskM = storageRefChildT.getDownloadUrl();
    String reference = storageConection.getReference().child("personalized/adios.jpeg").toString();

    boolean stop = true;

    protected ImageButton imgButton0;
    protected ImageButton imgButton1;
    protected ImageButton imgButton2;
    protected ImageButton imgButton3;
    protected ImageButton imgButton4;
    protected ImageButton imgButton5;
    protected ImageButton imgButton6;
    protected ImageButton imgButton7;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    UserInformation actualUserM = new UserInformation();

    public MisPictogramasFragment() {
        // Required empty public constructor
    }

    public static MisPictogramasFragment newInstance(String param1, String param2) {
        MisPictogramasFragment fragment = new MisPictogramasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        //Receive the object actual user.
        //actualUserM = (UserInformation) Intent.getIntentOld(NewPictogram.class).getSerializableExtra("user");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mis_pictogramas, container, false);

        imgButton0 = view.findViewById(R.id.imgButton0);
        imgButton1 = view.findViewById(R.id.imgButton1);
        imgButton2 = view.findViewById(R.id.imgButton2);
        imgButton3 = view.findViewById(R.id.imgButton3);
        imgButton4 = view.findViewById(R.id.imgButton4);
        imgButton5 = view.findViewById(R.id.imgButton5);
        imgButton6 = view.findViewById(R.id.imgButton6);
        imgButton7 = view.findViewById(R.id.imgButton7);

        //datos();

        /*realTimeDatabaseref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                try {
                    if (stop) //Prevent repeating "if (snapshot.exists())" indefinitely
                    {
                        if (snapshot.exists()) {
                            //if (task.isSuccessful()) {
                            Map<String, Object> map = new HashMap<>();
                            firebaseAuth = FirebaseAuth.getInstance();
                            String id = firebaseAuth.getCurrentUser().getUid();

                            if (id != null && !id.isEmpty()) {
                                //Search actual user data.
                                for (DataSnapshot ds : snapshot.getChildren()) {
                                    ds.getValue();
                                    actualUserM.setNombrePicto1(ds.child(id).getValue(UserInformation.class).getNombrePicto1());
                                    actualUserM.setNombrePicto2(ds.child(id).getValue(UserInformation.class).getNombrePicto2());
                                    actualUserM.setNombrePicto3(ds.child(id).getValue(UserInformation.class).getNombrePicto3());
                                    actualUserM.setNombrePicto4(ds.child(id).getValue(UserInformation.class).getNombrePicto4());
                                    actualUserM.setNombrePicto5(ds.child(id).getValue(UserInformation.class).getNombrePicto5());
                                    actualUserM.setNombrePicto6(ds.child(id).getValue(UserInformation.class).getNombrePicto6());
                                    actualUserM.setNombrePicto7(ds.child(id).getValue(UserInformation.class).getNombrePicto7());
                                    actualUserM.setNombrePicto8(ds.child(id).getValue(UserInformation.class).getNombrePicto8());
                                    actualUserM.setUrlPicto1(ds.child(id).getValue(UserInformation.class).getUrlPicto1());
                                    actualUserM.setUrlPicto2(ds.child(id).getValue(UserInformation.class).getUrlPicto2());
                                    actualUserM.setUrlPicto3(ds.child(id).getValue(UserInformation.class).getUrlPicto3());
                                    actualUserM.setUrlPicto4(ds.child(id).getValue(UserInformation.class).getUrlPicto4());
                                    actualUserM.setUrlPicto5(ds.child(id).getValue(UserInformation.class).getUrlPicto5());
                                    actualUserM.setUrlPicto6(ds.child(id).getValue(UserInformation.class).getUrlPicto6());
                                    actualUserM.setUrlPicto7(ds.child(id).getValue(UserInformation.class).getUrlPicto7());
                                    actualUserM.setUrlPicto8(ds.child(id).getValue(UserInformation.class).getUrlPicto8());
                                }
                            }
                            //}
                        }
                    }
                } catch (Exception e) {
                    final Toast toast = Toast.makeText(getActivity(), "Error de la aplicación.", Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                final Toast toast = Toast.makeText(getActivity(), "Error de la aplicación.", Toast.LENGTH_LONG);
            }
        });*/

        /*if (!actualUserM.getNombrePicto1().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto1())
                    .into(imgButton0);
        }
        if (!actualUserM.getNombrePicto2().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto2())
                    .into(imgButton1);
        }
        if (!actualUserM.getNombrePicto3().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto3())
                    .into(imgButton2);
        }
        if (!actualUserM.getNombrePicto4().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto4())
                    .into(imgButton3);
        }
        if (!actualUserM.getNombrePicto5().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto5())
                    .into(imgButton4);
        }
        if (!actualUserM.getNombrePicto6().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto6())
                    .into(imgButton5);
        }
        if (!actualUserM.getNombrePicto7().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto7())
                    .into(imgButton6);
        }
        if (!actualUserM.getNombrePicto8().equals(nombrePicto))
        {
            Glide.with(this)
                    .load(actualUserM.getUrlPicto8())
                    .into(imgButton7);
        }*/

        /*Glide.with(this)
                .load("https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/personalized%2FnameNew?alt=media&token=7a8acf3c-674a-4c5a-bc8f-5de3ec0c567c")
                .into(imgButton0);*/

        return view;
    }
    /*public void RecogerPictogramas (DataSnapshot dataSnapshot) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        UserID = user.getUid();
        Emailusers= user.getEmail();

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            uInfo.setEmail(ds.child(UserID).getValue(UserInformation.class).getEmail()); //set the email
            uInfo.setPassword(ds.child(UserID).getValue(UserInformation.class).getPassword()); //set the Password
            uInfo.setRol(ds.child(UserID).getValue(UserInformation.class).getRol()); //set the Rol
            uInfo.setRela(ds.child(UserID).getValue(UserInformation.class).getRela());//set the Rela
            Rela = uInfo.getRela();
            Rol = uInfo.getRol();

            uInfo.setNombrePicto1(ds.child(UserID).getValue(UserInformation.class).getNombrePicto1());
            uInfo.setNombrePicto2(ds.child(UserID).getValue(UserInformation.class).getNombrePicto2());
            uInfo.setNombrePicto3(ds.child(UserID).getValue(UserInformation.class).getNombrePicto3());
            uInfo.setNombrePicto4(ds.child(UserID).getValue(UserInformation.class).getNombrePicto4());
            uInfo.setNombrePicto5(ds.child(UserID).getValue(UserInformation.class).getNombrePicto5());
            uInfo.setNombrePicto6(ds.child(UserID).getValue(UserInformation.class).getNombrePicto6());
            uInfo.setNombrePicto7(ds.child(UserID).getValue(UserInformation.class).getNombrePicto7());
            uInfo.setNombrePicto8(ds.child(UserID).getValue(UserInformation.class).getNombrePicto8());
            uInfo.setUrlPicto1(ds.child(UserID).getValue(UserInformation.class).getUrlPicto1());
            uInfo.setUrlPicto2(ds.child(UserID).getValue(UserInformation.class).getUrlPicto2());
            uInfo.setUrlPicto3(ds.child(UserID).getValue(UserInformation.class).getUrlPicto3());
            uInfo.setUrlPicto4(ds.child(UserID).getValue(UserInformation.class).getUrlPicto4());
            uInfo.setUrlPicto5(ds.child(UserID).getValue(UserInformation.class).getUrlPicto5());
            uInfo.setUrlPicto6(ds.child(UserID).getValue(UserInformation.class).getUrlPicto6());
            uInfo.setUrlPicto7(ds.child(UserID).getValue(UserInformation.class).getUrlPicto7());
            uInfo.setUrlPicto8(ds.child(UserID).getValue(UserInformation.class).getUrlPicto8());
            Relacion1= uInfo.getRela();
            if (Relacion1.equals(Emailusers) && Rol.equals("administrador") ){
            nombrePicto1 = uInfo.getNombrePicto1();

            urlPicto1 = uInfo.getUrlPicto1();
            nombrePicto2 = uInfo.getNombrePicto2();
            urlPicto2 = uInfo.getUrlPicto2();
            nombrePicto3 = uInfo.getNombrePicto3();
            urlPicto3 = uInfo.getUrlPicto3();
            nombrePicto4 = uInfo.getNombrePicto4();
            urlPicto4 = uInfo.getUrlPicto4();
            nombrePicto5 = uInfo.getNombrePicto5();
            urlPicto5 = uInfo.getUrlPicto5();
            nombrePicto6 = uInfo.getNombrePicto6();
            urlPicto6 = uInfo.getUrlPicto6();
            nombrePicto7 = uInfo.getNombrePicto7();
            urlPicto7 = uInfo.getUrlPicto7();
            nombrePicto8 = uInfo.getNombrePicto8();
            urlPicto8 = uInfo.getUrlPicto8();
        }
        }
    }*/

    public void datos()
    {
        realTimeDatabaseref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserInformation userIn = snapshot.getValue(UserInformation.class);
                /*try {
                    if (stop) //Prevent repeating "if (snapshot.exists())" indefinitely
                    {
                        if (snapshot.exists()) {
                            //if (task.isSuccessful()) {
                            Map<String, Object> map = new HashMap<>();
                            firebaseAuth = FirebaseAuth.getInstance();
                            String id = firebaseAuth.getCurrentUser().getUid();

                            if (id != null && !id.isEmpty()) {
                                //Search actual user data.
                                for (DataSnapshot ds : snapshot.getChildren()) {
                                    ds.getValue();
                                    actualUserM.setNombrePicto1(ds.child(id).getValue(UserInformation.class).getNombrePicto1());
                                    actualUserM.setNombrePicto2(ds.child(id).getValue(UserInformation.class).getNombrePicto2());
                                    actualUserM.setNombrePicto3(ds.child(id).getValue(UserInformation.class).getNombrePicto3());
                                    actualUserM.setNombrePicto4(ds.child(id).getValue(UserInformation.class).getNombrePicto4());
                                    actualUserM.setNombrePicto5(ds.child(id).getValue(UserInformation.class).getNombrePicto5());
                                    actualUserM.setNombrePicto6(ds.child(id).getValue(UserInformation.class).getNombrePicto6());
                                    actualUserM.setNombrePicto7(ds.child(id).getValue(UserInformation.class).getNombrePicto7());
                                    actualUserM.setNombrePicto8(ds.child(id).getValue(UserInformation.class).getNombrePicto8());
                                    actualUserM.setUrlPicto1(ds.child(id).getValue(UserInformation.class).getUrlPicto1());
                                    actualUserM.setUrlPicto2(ds.child(id).getValue(UserInformation.class).getUrlPicto2());
                                    actualUserM.setUrlPicto3(ds.child(id).getValue(UserInformation.class).getUrlPicto3());
                                    actualUserM.setUrlPicto4(ds.child(id).getValue(UserInformation.class).getUrlPicto4());
                                    actualUserM.setUrlPicto5(ds.child(id).getValue(UserInformation.class).getUrlPicto5());
                                    actualUserM.setUrlPicto6(ds.child(id).getValue(UserInformation.class).getUrlPicto6());
                                    actualUserM.setUrlPicto7(ds.child(id).getValue(UserInformation.class).getUrlPicto7());
                                    actualUserM.setUrlPicto8(ds.child(id).getValue(UserInformation.class).getUrlPicto8());
                                }
                            }
                            //}
                        }
                    }
                } catch (Exception e) {
                    final Toast toast = Toast.makeText(getActivity(), "Error de la aplicación.", Toast.LENGTH_LONG);
                }*/
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
                final Toast toast = Toast.makeText(getActivity(), "Error de la aplicación.", Toast.LENGTH_LONG);
            }
        });
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ComunicationFragments) {
            cListener = (ComunicationFragments) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " Must be implement ComunicationFragments");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cListener = null;
    }
}
