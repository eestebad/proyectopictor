package com.example.pictor;

public class Pictogram {
    private int idPicto;
    private String pictoName;
    private String urlStorage;

    public Pictogram() {
    }

    public Pictogram(int idPicto, String pictoName, String urlStorage) {
        this.idPicto = idPicto;
        this.pictoName = pictoName;
        this.urlStorage = urlStorage;
    }

    public int getIdPicto() {return idPicto;}

    public String getPictoName() {
        return pictoName;
    }

    public String getUrlStorage() {
        return urlStorage;
    }
}
