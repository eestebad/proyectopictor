package com.example.pictor;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


public class Register extends AppCompatActivity implements  View.OnClickListener{

// ...
// Initialize Firebase Auth
       // etemail = (EditText) findViewById(R.id.email);
        //etpassword = (EditText) findViewById(R.id.password);
        //etoption1= (radiogroup) findViewById(R.id.opcion1);
        //etoption2= (EditText) findViewById(R.id.opcion2);


    private EditText TextEmail;
    private EditText TextPassword;
    private EditText TextRelacion;
    private Button btnRegistrar;
    private ProgressDialog progressDialog;
    private RadioButton r1,r2;
    private DatabaseReference RTDatabase;
    public String relaaux;
    public DataSnapshot dataSnapshot;

    UserInformation user = new UserInformation();
    //Declaramos un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        //inicializamos el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();
        RTDatabase = FirebaseDatabase.getInstance().getReference();

        //Referenciamos los views
        TextEmail = (EditText) findViewById(R.id.email);
        TextPassword = (EditText) findViewById(R.id.password);
        TextRelacion = (EditText) findViewById(R.id.relacion);
        r1=(RadioButton)findViewById(R.id.opcion1);
        r2=(RadioButton)findViewById(R.id.opcion2);

        btnRegistrar = (Button) findViewById(R.id.registrocompletado);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        btnRegistrar.setOnClickListener(this);
    }

    private void registrarUsuario(){

        //Obtenemos el email y la contraseña desde las cajas de texto
        String email = TextEmail.getText().toString().trim();//sin espacios
        String password  = TextPassword.getText().toString().trim();
        String rela = TextRelacion.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Se debe ingresar un email",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Falta ingresar la contraseña",Toast.LENGTH_LONG).show();
            return;
        }

        progressDialog.setMessage("Realizando registro en linea...");
        progressDialog.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        String usuario= "usuario";
                        String admin= "administrador";
                        String nombrePicto1 = "nameNew";
                        String urlPicto1 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto2 = "nameNew";
                        String urlPicto2 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto3 = "nameNew";
                        String urlPicto3 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto4 = "nameNew";
                        String urlPicto4 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto5 = "nameNew";
                        String urlPicto5 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto6 = "nameNew";
                        String urlPicto6 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto7 = "nameNew";
                        String urlPicto7 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";
                        String nombrePicto8 = "nameNew";
                        String urlPicto8 = "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/nuevo.png?alt=media&token=103f8619-417f-47f7-a465-fd9383e6dfa2";

                        if(task.isSuccessful()){
                            String id = firebaseAuth.getCurrentUser().getUid();

                            Map<String, Object> map = new HashMap<>();
                            map.put("email", email);
                            map.put("password", password);
                            map.put("Relacion", rela) ;
                            map.put("nombrePicto1", nombrePicto1);
                            map.put("urlPicto1", urlPicto1);
                            map.put("nombrePicto2", nombrePicto2);
                            map.put("urlPicto2", urlPicto2);
                            map.put("nombrePicto3", nombrePicto3);
                            map.put("urlPicto3", urlPicto3);
                            map.put("nombrePicto4", nombrePicto4);
                            map.put("urlPicto4", urlPicto4);
                            map.put("nombrePicto5", nombrePicto5);
                            map.put("urlPicto5", urlPicto5);
                            map.put("nombrePicto6", nombrePicto6);
                            map.put("urlPicto6", urlPicto6);
                            map.put("nombrePicto7", nombrePicto7);
                            map.put("urlPicto7", urlPicto7);
                            map.put("nombrePicto8", nombrePicto8);
                            map.put("urlPicto8", urlPicto8);




                            if(r1.isChecked()==true) {
                                map.put("Rol", usuario);
                                RTDatabase.child("People").child(id).setValue(map);
                                Toast.makeText(Register.this, "Se ha registrado el usuario con el email: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
                            }
                            else if(r2.isChecked()==true){
                                map.put("Rol", admin);
                                RTDatabase.child("People").child(id).setValue(map);
                                Toast.makeText(Register.this, "Se ha registrado el usuario con el email: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
                            }


                         }else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                                Toast.makeText(Register.this, "Ese usuario ya existe ", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Register.this, "No se pudo registrar el usuario, al menos 6 caracteres en contraseña e email correcto ", Toast.LENGTH_LONG).show();
                            }
                        }
                        progressDialog.dismiss();
                    }
                });
    }


    @Override
    public void onClick(View v) {
        registrarUsuario();
        Intent intentregistracioncompleted = new Intent(Register.this, MainActivity.class);
        startActivity(intentregistracioncompleted);
        finish();


    }
}


