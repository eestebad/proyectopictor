package com.example.pictor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener{

    private static final String TAG ="" ;
    private EditText TextEmail;
    private EditText TextPassword;
    private Button  btnLogin;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String UserID;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference();
    private String Rol;
    public UserInformation uInfo = new UserInformation();
    public String nombrePicto1;
    public String urlPicto1;
    public String nombrePicto2;
    public String urlPicto2;
    public String nombrePicto3;
    public String urlPicto3;
    public String nombrePicto4;
    public String urlPicto4;
    public String nombrePicto5;
    public String urlPicto5;
    public String nombrePicto6;
    public String urlPicto6;
    public String nombrePicto7;
    public String urlPicto7;
    public String nombrePicto8;
    public String urlPicto8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

// Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAuth = FirebaseAuth.getInstance();


        //Referenciamos los views

        TextEmail = (EditText) findViewById(R.id.etEmailAdd);
        TextPassword = (EditText) findViewById(R.id.etPasswordAdd);
        btnLogin = (Button) findViewById(R.id.Login);
        progressDialog = new ProgressDialog(this);

        //asociamos un oyente al evento clic del botón
        btnLogin.setOnClickListener(this);


    }


    public void initVentanaOpciones(View view) {
        //Declaración del Intent explícito para cambiar de ventana
        Intent intentVentanaOpciones = new Intent(this, Options.class);
        //Ejecución del Intent:
        startActivity(intentVentanaOpciones);
        finish();
    }

    public void initregistracion(View view) {
        //Declaración del Intent explícito para cambiar de ventana
        Intent intentregistracion = new Intent(this, Register.class);
        //Ejecución del Intent:
        startActivity(intentregistracion);
        finish();
    }
    private void loguearUsuario() {
        //Obtenemos el email y la contraseña desde las cajas de texto
        final String email = TextEmail.getText().toString().trim();
        String password = TextPassword.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {//(precio.equals(""))
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Falta ingresar la contraseña", Toast.LENGTH_LONG).show();
            return;
        }


        progressDialog.setMessage("Realizando consulta en linea...");
        progressDialog.show();

        //loguear usuario
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {


                            ref.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                     
                                    if (snapshot.exists()) {
                                        RecogerDatos(snapshot);
                                    }
                                }


                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {
                                    Log.w(TAG, "The read failed: " + error.toException());
                                }
                            });

                        } else {
                        if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                            Toast.makeText(MainActivity.this, "Ese usuario ya existe ", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(MainActivity.this, "No se pudo registrar el usuario ", Toast.LENGTH_LONG).show();
                            }
                        }
                        progressDialog.dismiss();
                    }
    });

    }

public void RecogerDatos (DataSnapshot dataSnapshot){
        FirebaseUser user = firebaseAuth.getCurrentUser();
        UserID = user.getUid();

    //UserInformation uInfo = new UserInformation();
        for(DataSnapshot ds : dataSnapshot.getChildren()){
            uInfo.setEmail(ds.child(UserID).getValue(UserInformation.class).getEmail()); //set the email
            uInfo.setPassword(ds.child(UserID).getValue(UserInformation.class).getPassword()); //set the Password
            uInfo.setRol(ds.child(UserID).getValue(UserInformation.class).getRol()); //set the Rol
            Rol = uInfo.getRol();
            //Set the pictograms.
            uInfo.setNombrePicto1(ds.child(UserID).getValue(UserInformation.class).getNombrePicto1());
            uInfo.setNombrePicto2(ds.child(UserID).getValue(UserInformation.class).getNombrePicto2());
            uInfo.setNombrePicto3(ds.child(UserID).getValue(UserInformation.class).getNombrePicto3());
            uInfo.setNombrePicto4(ds.child(UserID).getValue(UserInformation.class).getNombrePicto4());
            uInfo.setNombrePicto5(ds.child(UserID).getValue(UserInformation.class).getNombrePicto5());
            uInfo.setNombrePicto6(ds.child(UserID).getValue(UserInformation.class).getNombrePicto6());
            uInfo.setNombrePicto7(ds.child(UserID).getValue(UserInformation.class).getNombrePicto7());
            uInfo.setNombrePicto8(ds.child(UserID).getValue(UserInformation.class).getNombrePicto8());
            uInfo.setUrlPicto1(ds.child(UserID).getValue(UserInformation.class).getUrlPicto1());
            uInfo.setUrlPicto2(ds.child(UserID).getValue(UserInformation.class).getUrlPicto2());
            uInfo.setUrlPicto3(ds.child(UserID).getValue(UserInformation.class).getUrlPicto3());
            uInfo.setUrlPicto4(ds.child(UserID).getValue(UserInformation.class).getUrlPicto4());
            uInfo.setUrlPicto5(ds.child(UserID).getValue(UserInformation.class).getUrlPicto5());
            uInfo.setUrlPicto6(ds.child(UserID).getValue(UserInformation.class).getUrlPicto6());
            uInfo.setUrlPicto7(ds.child(UserID).getValue(UserInformation.class).getUrlPicto7());
            uInfo.setUrlPicto8(ds.child(UserID).getValue(UserInformation.class).getUrlPicto8());

            /*nombrePicto1 = uInfo.getNombrePicto1();
            urlPicto1 = uInfo.getUrlPicto1();
            nombrePicto2 = uInfo.getNombrePicto2();
            urlPicto2 = uInfo.getUrlPicto2();
            nombrePicto3 = uInfo.getNombrePicto3();
            urlPicto3 = uInfo.getUrlPicto3();
            nombrePicto4 = uInfo.getNombrePicto4();
            urlPicto4 = uInfo.getUrlPicto4();
            nombrePicto5 = uInfo.getNombrePicto5();
            urlPicto5 = uInfo.getUrlPicto5();
            nombrePicto6 = uInfo.getNombrePicto6();
            urlPicto6 = uInfo.getUrlPicto6();
            nombrePicto7 = uInfo.getNombrePicto7();
            urlPicto7 = uInfo.getUrlPicto7();
            nombrePicto8 = uInfo.getNombrePicto8();
            urlPicto8 = uInfo.getUrlPicto8();*/

            //Send the user object to the NewPictogramActivity.
            /*Intent intent = new Intent(MainActivity.this, NewPictogram.class);
            intent.putExtra("user", uInfo);
            startActivity(intent);*/

            Intent intent = new Intent();
            intent.putExtra("user", uInfo);
            setResult(Activity.RESULT_OK,intent);


        }
    if (Rol.equals("administrador")) {
        Toast.makeText(MainActivity.this, "Bienvenido: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
        Intent GoToOpciones = new Intent(this, Options.class);
        startActivity(GoToOpciones);
    } else if (Rol.equals("usuario") ) {
        Toast.makeText(MainActivity.this, "Bienvenido: " + TextEmail.getText(), Toast.LENGTH_LONG).show();
        Intent GoToTranslator = new Intent(this, Translator.class);
        startActivity(GoToTranslator);
    }



}

    // int pos = email.indexOf("@");
                            // String user = email.substring(0, pos);
                             //user = firebaseAuth.getCurrentUser();


    public void onClick(View view) {
        loguearUsuario();

    }
    public void InicioSinUsuario(View view){
        Intent GoToTranslator = new Intent(this, Translator.class);
        Toast.makeText(MainActivity.this,"Al loggear sin usuario no contarás con pictogramas personalizados.\nMuchas gracias por usar la APP",Toast.LENGTH_LONG).show();
        startActivity(GoToTranslator);

    }
}
