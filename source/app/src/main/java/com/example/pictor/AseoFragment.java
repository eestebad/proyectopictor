package com.example.pictor;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AseoFragment extends Fragment {

    private ComunicationFragments cListener;

    public ImageButton duchar;
    public ImageButton orinar;
    public ImageButton lavarManos;
    public ImageButton lavarDientes;
    public ImageButton cuartoAseo;
    public ImageButton peluqueria;
    public ImageButton peinar;
    public ImageButton lavarCara;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AseoFragment() {
        // Required empty public constructor
    }

    public static AseoFragment newInstance(String param1, String param2) {
        AseoFragment fragment = new AseoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aseo, container, false);

        //Initialize the ImageButtons.
        duchar = view.findViewById(R.id.imageBtnDuchar);
        orinar = view.findViewById(R.id.imageBtnOrinar);
        lavarManos = view.findViewById(R.id.imageBtnLavarManos);
        lavarDientes = view.findViewById(R.id.imageBtnDientes);
        cuartoAseo = view.findViewById(R.id.imageBtnCuartoAseo);
        peluqueria = view.findViewById(R.id.imageBtnPeluqueria);
        peinar = view.findViewById(R.id.imageBtnPeinar);
        lavarCara = view.findViewById(R.id.imageBtnLavarCara);

        //Initialize the ArrayList of ImageButtons.
        List<ImageButton> imageBtnAseoList = new ArrayList<>();
        imageBtnAseoList.add(duchar);
        imageBtnAseoList.add(orinar);
        imageBtnAseoList.add(lavarManos);
        imageBtnAseoList.add(lavarDientes);
        imageBtnAseoList.add(cuartoAseo);
        imageBtnAseoList.add(peluqueria);
        imageBtnAseoList.add(peinar);
        imageBtnAseoList.add(lavarCara);

            //Initialize the ArrayList of Aseo Pictograms.
            List<Pictogram> pictogramAseoList = new ArrayList<>();
            pictogramAseoList.add(new Pictogram(0, "duchar ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fduchar.png?alt=media&token=4fd8b967-b8eb-45e6-abd9-a69b7cf751ac"));
            pictogramAseoList.add(new Pictogram(1, "hacer pis ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Ftener%20ganas%20de%20hacer%20pis.png?alt=media&token=6158e3c5-1112-49d6-9bd5-de85b3f842d3"));
            pictogramAseoList.add(new Pictogram(2, "lavar las manos ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2FlavarManos.png?alt=media&token=cfac926d-b180-46fb-b065-d8d4776c1321"));
            pictogramAseoList.add(new Pictogram(3, "lavar los dientes ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fcepillar%20los%20dientes.png?alt=media&token=430328aa-b14b-4107-8406-8eebbccf7d1f"));
            pictogramAseoList.add(new Pictogram(3, "cuarto de aseo ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fcuartos%20de%20aseo.png?alt=media&token=95ab5550-1e68-43a6-9153-1ed33c11d50c"));
            pictogramAseoList.add(new Pictogram(3, "peluquería ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fpeluquer%C3%ADa.png?alt=media&token=8e3fd0d5-f9d1-431d-8628-a6e7e779a6aa"));
            pictogramAseoList.add(new Pictogram(3, "peinar ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2Fpeinar.png?alt=media&token=ffcdacb4-3512-4173-a6e8-00739dcf14a6"));
            pictogramAseoList.add(new Pictogram(3, "lavar la cara ", "https://firebasestorage.googleapis.com/v0/b/pictor-46798.appspot.com/o/aseo%2FlavarCara.png?alt=media&token=23b94c78-7606-4de2-a0c8-67d7500e3d27"));

        int count = 0;

            //Create each ImageButton with a Pictogram.
            for (ImageButton imageButton:imageBtnAseoList)
            {
                try {
                    Glide.with(this)
                            .load(pictogramAseoList.get(count).getUrlStorage())
                            .into(imageButton);
                    count++;
                }catch(Exception exception)
                {
                    Log.e("Error", "Fallo en la aplicación");
                }
            }

        duchar.setOnClickListener(v -> cListener.showTextImage(v));
        orinar.setOnClickListener(v -> cListener.showTextImage(v));
        lavarManos.setOnClickListener(v -> cListener.showTextImage(v));
        lavarDientes.setOnClickListener(v -> cListener.showTextImage(v));
        cuartoAseo.setOnClickListener(v -> cListener.showTextImage(v));
        peluqueria.setOnClickListener(v -> cListener.showTextImage(v));
        peinar.setOnClickListener(v -> cListener.showTextImage(v));
        lavarCara.setOnClickListener(v -> cListener.showTextImage(v));

        return view;
    }

    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ComunicationFragments) {
            cListener = (ComunicationFragments) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " Must be implement ComunicationFragments");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cListener = null;
    }
}
