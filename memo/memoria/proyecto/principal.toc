\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducción}{1}{}%
\contentsline {section}{\numberline {1.1}Presentación}{1}{}%
\contentsline {subsection}{\numberline {1.1.1}¿Que es PictorSale?}{1}{}%
\contentsline {subsection}{\numberline {1.1.2}Motivación}{3}{}%
\contentsline {subsection}{\numberline {1.1.3}Diferencias con otras Apps}{4}{}%
\contentsline {subsection}{\numberline {1.1.4}Objetivos}{5}{}%
\contentsline {chapter}{\numberline {2}Exposición}{7}{}%
\contentsline {section}{\numberline {2.1}Organización}{8}{}%
\contentsline {section}{\numberline {2.2}Paso a paso}{9}{}%
\contentsline {section}{\numberline {2.3}Aprendizajes}{10}{}%
\contentsline {chapter}{\numberline {3}Tecnologías y desarrollo}{11}{}%
\contentsline {section}{\numberline {3.1}Tecnologias}{11}{}%
\contentsline {subsection}{\numberline {3.1.1}Lenguajes y herramientas utilizados}{11}{}%
\contentsline {subsection}{\numberline {3.1.2}Base de datos}{11}{}%
\contentsline {subsection}{\numberline {3.1.3}Librerias}{12}{}%
\contentsline {subsection}{\numberline {3.1.4}Repositorio}{12}{}%
\contentsline {section}{\numberline {3.2}Desarrollo}{12}{}%
\contentsline {subsection}{\numberline {3.2.1}Prototype}{12}{}%
\contentsline {subsection}{\numberline {3.2.2}Estructura de la App }{12}{}%
\contentsline {subsection}{\numberline {3.2.3}Pantallas de Inicio, Registro y Traductor}{13}{}%
\contentsline {subsection}{\numberline {3.2.4}Almacenamiento de usuarios y pictogramas}{14}{}%
\contentsline {subsection}{\numberline {3.2.5}Comunicación de fragments.}{14}{}%
\contentsline {subsection}{\numberline {3.2.6}Refactorización de código.}{15}{}%
\contentsline {subsection}{\numberline {3.2.7}Problemas encontrados y soluciones técnicas.}{15}{}%
\contentsline {subsection}{\numberline {3.2.8}Seguridad}{15}{}%
\contentsline {subsection}{\numberline {3.2.9}Futuras actualizaciones}{15}{}%
\contentsline {chapter}{\numberline {4}Conclusiones}{17}{}%
\contentsline {section}{\numberline {4.1}Grado de Consecución de los Objetivos}{17}{}%
\contentsline {section}{\numberline {4.2}Reflexión}{18}{}%
\contentsline {chapter}{\numberline {5}Anexos}{19}{}%
\contentsline {section}{\numberline {5.1}Glosario}{19}{}%
\contentsline {section}{\numberline {5.2}Código}{20}{}%
\contentsline {chapter}{\numberline {6}Bibliografía}{21}{}%
